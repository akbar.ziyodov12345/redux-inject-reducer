export function isString(value) {
    return typeof value === "string";
}
export function isFunction(value) {
    return typeof value === "function";
}
export function isUndefined(value) {
    return typeof value === "undefined";
}
export function has(value, property) {
    return Object.prototype.hasOwnProperty.call(value, property);
}
export function isPlainObject(value) {
    return Object.prototype.toString.call(value) === "[object Object]";
}
//# sourceMappingURL=Utils.js.map