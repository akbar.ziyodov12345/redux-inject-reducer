import { createChangeEmitter } from "change-emitter";
import { has, isFunction, isPlainObject, isString, isUndefined } from "./Utils";
function getUndefinedStateErrorMessage(key, action) {
    var actionType = action && action.type;
    var actionName = (actionType && "\"" + actionType.toString() + "\"") || "an action";
    return ("Given action " + actionName + ", reducer \"" + key + "\" returned undefined. " +
        "To ignore an action, you must explicitly return the previous state. " +
        "If you want this reducer to hold no value, you can return null instead of undefined.");
}
function assertReducerShape(reducers) {
    reducers.forEach(function (reducer, name) {
        var initialState = reducer(undefined, { type: Math.random().toString(36) });
        if (isUndefined(initialState)) {
            throw new Error("Reducer \"" + name + "\" returned undefined during initialization. " +
                "If the state passed to the reducer is undefined, you must " +
                "explicitly return the initial state. The initial state may " +
                "not be undefined. If you don't want to set a value for this reducer, " +
                "you can use null instead of undefined.");
        }
    });
}
var Context = (function () {
    function Context(options) {
        var _this = this;
        this.emitter = createChangeEmitter();
        this.reducers = new Map();
        this.ejectedReducers = new Set();
        if (options && options.reducers) {
            Object.keys(options.reducers).forEach(function (name) {
                var reducer = options.reducers[name];
                if (!isFunction(reducer)) {
                    throw new Error("Invalid reducer of type \"" + typeof reducer + "\" provided for key \"" + name + "\".");
                }
                _this.reducers.set(name, options.reducers[name]);
            });
        }
    }
    Context.prototype.createSelector = function (name) {
        return function stateSelector(rootState) {
            if (!isPlainObject(rootState)) {
                throw new Error("Trying to obtain state value for \"" + name + "\" from non plain object root state.");
            }
            if (!has(rootState, name)) {
                throw new Error("Root state does not \"" + name + "\" state. " +
                    ("Most likely you didn't replace new root reducer after injecting \"" + name + "\" reducer."));
            }
            return rootState[name];
        };
    };
    Context.prototype.injectReducer = function (name, reducer) {
        if (!isString(name)) {
            throw new Error("Provided invalid reducer \"name\" of type \"" + typeof name + "\".");
        }
        if (!isFunction(reducer)) {
            throw new Error("Invalid reducer of type \"" + typeof reducer + "\" provided for key \"" + name + "\".");
        }
        this.reducers.set(name, reducer);
        this.ejectedReducers.delete(name);
        this.emitter.emit();
        return this.createSelector(name);
    };
    Context.prototype.ejectReducer = function (name) {
        if (!isString(name)) {
            throw new Error("Provided invalid reducer \"name\" of type \"" + typeof name + "\".");
        }
        this.ejectedReducers.add(name);
        if (this.reducers.delete(name)) {
            this.emitter.emit();
        }
    };
    Context.prototype.subscribe = function (listener) {
        return this.emitter.listen(listener);
    };
    Context.prototype.combineReducers = function () {
        var reducers = new Map(this.reducers);
        var ejectedReducers = new Set(this.ejectedReducers);
        /* istanbul ignore else  */
        if (process.env.NODE_ENV !== "production") {
            if (reducers.size === 0) {
                console.error("There are no reducers in context.");
            }
        }
        var shapeAssertionError;
        try {
            assertReducerShape(reducers);
        }
        catch (e) {
            shapeAssertionError = e;
        }
        return function rootReducer(rootState, action) {
            if (rootState === void 0) { rootState = {}; }
            if (shapeAssertionError) {
                throw shapeAssertionError;
            }
            var nextRootState = {};
            Object.keys(rootState).forEach(function (name) {
                if (!ejectedReducers.has(name)) {
                    nextRootState[name] = rootState[name];
                }
            });
            var changed = false;
            reducers.forEach(function (reducer, name) {
                var state = rootState[name];
                var nextState = reducer(state, action);
                if (isUndefined(nextState)) {
                    throw new Error(getUndefinedStateErrorMessage(name, action));
                }
                nextRootState[name] = nextState;
                changed = changed || state !== nextState;
            });
            return changed ? nextRootState : rootState;
        };
    };
    Context.prototype.syncWithStore = function (store) {
        var _this = this;
        return this.subscribe(function () { return store.replaceReducer(_this.combineReducers()); });
    };
    return Context;
}());
export { Context };
//# sourceMappingURL=Context.js.map