# redux-inject-reducer


[![build status](https://img.shields.io/travis/umidbekkarimov/redux-inject-reducer/master.svg?style=flat-square)](https://travis-ci.org/umidbekkarimov/redux-inject-reducer)
[![npm version](https://img.shields.io/npm/v/redux-inject-reducer.svg?style=flat-square)](https://www.npmjs.com/package/redux-inject-reducer)
[![npm downloads](https://img.shields.io/npm/dm/redux-inject-reducer.svg?style=flat-square)](https://www.npmjs.com/package/redux-inject-reducer)
[![Codecov](https://img.shields.io/codecov/c/gh/umidbekkarimov/redux-inject-reducer.svg?style=flat-square)](https://codecov.io/gh/umidbekkarimov/redux-inject-reducer)

