"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isString(value) {
    return typeof value === "string";
}
exports.isString = isString;
function isFunction(value) {
    return typeof value === "function";
}
exports.isFunction = isFunction;
function isUndefined(value) {
    return typeof value === "undefined";
}
exports.isUndefined = isUndefined;
function has(value, property) {
    return Object.prototype.hasOwnProperty.call(value, property);
}
exports.has = has;
function isPlainObject(value) {
    return Object.prototype.toString.call(value) === "[object Object]";
}
exports.isPlainObject = isPlainObject;
//# sourceMappingURL=Utils.js.map