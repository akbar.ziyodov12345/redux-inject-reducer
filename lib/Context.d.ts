import { Reducer, Store } from "redux";
export declare type Selector<S> = (state: any) => S;
export declare class Context<R extends {
    [name: string]: any;
}> {
    private emitter;
    private reducers;
    private ejectedReducers;
    constructor(options?: {
        reducers: {
            [name: string]: Reducer<any>;
        };
    });
    createSelector<N extends keyof R>(name: N): (rootState: R) => R[N];
    injectReducer<N extends keyof R, S>(name: N, reducer: Reducer<S>): Selector<S>;
    ejectReducer<N extends keyof R>(name: N): void;
    subscribe(listener: () => void): () => void;
    combineReducers(): Reducer<R>;
    syncWithStore(store: Store<R>): () => void;
}
