export declare function isString(value: any): boolean;
export declare function isFunction(value: any): boolean;
export declare function isUndefined(value: any): boolean;
export declare function has(value: any, property: string): any;
export declare function isPlainObject(value: any): boolean;
